// If an li element is clicked, toggle the class "done" on the <li>
const list = document.querySelector('ul');
list.addEventListener('click', function(e) {
  if (e.target.tagName.toLowerCase() === 'li') {
    e.target.classList.toggle('done');
    } else {
      e.target.parentNode.classList.toggle('done');
    }
  });
 
// If a delete link is clicked, delete the li element / remove from the DOM
const deleteItem = document.getElementsByClassName('delete')[0];
deleteItem.addEventListener('click', function () {
  deleteItem.parentNode.remove();
});

// If an 'Add' link is clicked, adds the item as a new list item with
// addListItem function has been started to help you get going!
// Make sure to add an event listener(s) to your new <li> (if needed)
const addListItem = function(e) {
  e.preventDefault();
  const input = this.parentNode.getElementsByTagName('input')[0];
  const text = input.value;
  const li = document.createElement('li');
  const textNode = document.createTextNode(text);
  list.appendChild(li);
  const span = document.createElement('span');
  span.appendChild(textNode);
  li.appendChild(span);
  const element = document.createElement('a');
  li.appendChild(element);
  element.setAttribute('class', 'delete');
  const deleteButton = document.createTextNode('Delete');
  element.appendChild(deleteButton);
 
  element.addEventListener('click', function () {
    element.parentNode.remove();
  });
};

const addItem = document.getElementsByClassName('add-item')[0];
addItem.addEventListener('click', addListItem);